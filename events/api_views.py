import json
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Conference, Location, State
from .acls import get_picture_url, get_weather_data
import requests

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder
    }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        response_data = {
            "conferences": list(conferences),
        }
        return JsonResponse(response_data, encoder=ConferenceListEncoder)
    elif request.method == "POST":
        content = json.loads(request.body)
        location_id = content.get("location")
        try:
            location = Location.objects.get(id=location_id)
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        content.pop("location")
        conference = Conference.objects.create(location=location, **content)
        conference_data = {
            "name": conference.name,
            "starts": conference.starts,
            "ends": conference.ends,
            "description": conference.description,
            "created": conference.created,
            "updated": conference.updated,
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "name": conference.location.name,
                "href": conference.location.get_api_url()
            }
        }

        return JsonResponse(conference_data, encoder=ConferenceDetailEncoder, safe=False)

@require_http_methods(["GET", "PUT", "PATCH"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Conference not found"}, status=404)

    if request.method == "GET":
        weather_data = get_weather_data(conference.location.city, conference.location.state.abbreviation)
        conference_data = {
            "conference": {
                "name": conference.name,
                "starts": conference.starts,
                "ends": conference.ends,
                "description": conference.description,
                "created": conference.created,
                "updated": conference.updated,
                "max_presentations": conference.max_presentations,
                "max_attendees": conference.max_attendees,
                "location": {
                    "name": conference.location.name,
                    "href": conference.location.get_api_url()
                }
            },
            "weather": weather_data,
        }
        return JsonResponse(conference_data, encoder=ConferenceDetailEncoder, safe=False)
    elif request.method == "PUT" or request.method == "PATCH":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(conference, key, value)
        if "location" in content:
            try:
                location = Location.objects.get(id=content["location"])
                conference.location = location
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )
        conference.save()
        return JsonResponse(
            {"message": "Conference updated successfully"},
            status=200
        )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
        location_data = []
        for location in locations:
            photo_data = get_photo(location.city, location.state.abbreviation)
            location_data.append({
                "name": location.name,
                "picture_url": photo_data["picture_url"],
                "href": location.get_api_url(),
            })
        return JsonResponse({"locations": location_data})
    elif request.method == "POST":
        content = json.loads(request.body)

    try:
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )

    # Get picture URL from Pexels API
    photo_data = get_picture_url(f'{content["city"]}')
    picture_url = photo_data.get("picture_url")

    # Add picture URL to the content dictionary
    content["picture_url"] = picture_url

    location = Location.objects.create(**content)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse({"deleted": False}, status=404)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

@require_http_methods(["POST"])
def api_create_conference(request):
    if request.method == "POST":
        content = json.loads(request.body)
        location_id = content.get("location")
        try:
            location = Location.objects.get(id=location_id)
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        content.pop("location")

        conference = Conference.objects.create(location=location, **content)

        conference_data = {
            "name": conference.name,
            "starts": conference.starts,
            "ends": conference.ends,
            "description": conference.description,
            "created": conference.created,
            "updated": conference.updated,
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "name": conference.location.name,
                "href": conference.location.get_api_url()
            }
        }
        return JsonResponse(conference_data, encoder=ConferenceDetailEncoder, safe=False)


@require_http_methods(["PUT"])
def api_update_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Conference not found"}, status=404)

    if request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(conference, key, value)
        if "location" in content:
            try:
                location = Location.objects.get(id=content["location"])
                conference.location = location
            except Location.DoesNotExist:
                return JsonResponse({"message": "Invalid location id"}, status=400)
        conference.save()
        return JsonResponse({"message": "Conference updated successfully"}, status=200)

@require_http_methods(["DELETE"])
def api_delete_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Conference not found"}, status=404)

    conference.delete()
    return JsonResponse({"message": "Conference deleted successfully"}, status=204)
