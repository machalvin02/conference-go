from django.urls import path

from .api_views import (
    api_list_conferences,
    api_list_locations,
    api_show_conference,
    api_show_location,
    api_update_conference,
    api_delete_conference,
)


urlpatterns = [
    path("conferences/", api_list_conferences, name="api_list_conferences"),
    path(
        "conferences/<int:id>/",
        api_show_conference,
        name="api_show_conference",
    ),
    path("conferences/<int:id>/update/", api_update_conference, name="api_update_conference"),
    path("conferences/<int:id>/delete/", api_delete_conference, name="api_delete_conference"),
    path("locations/", api_list_locations, name="api_list_locations"),
    path("locations/<int:id>/", api_show_location, name="api_show_location"),
]
