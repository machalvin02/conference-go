import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_picture_url(query):
    """
    Get the URL of a picture from the Pexels API.
    """
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        response_data = response.json()
        if "photos" in response_data and len(response_data["photos"]) > 0:
            picture_url = response_data["photos"][0]["src"]["original"]
            return {"picture_url": picture_url}
    else:
        return {"picture_url": None}

def get_weather_data(city, state):
    geocoding_url = f"https://api.openweathermap.org/data/2.5/weather"
    geocoding_params = {
        "q": f"{city},{state}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_response = requests.get(geocoding_url, params=geocoding_params)
    geocoding_data = geocoding_response.json()

    if geocoding_response.status_code != 200:
        return {"weather": None}

    latitude = geocoding_data["coord"]["lat"]
    longitude = geocoding_data["coord"]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/onecall"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "exclude": "minutely,hourly,daily",
        "appid": OPEN_WEATHER_API_KEY,
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather_data = weather_response.json()

    if weather_response.status_code != 200:
        return {"weather": None}

    main_temp = weather_data["current"]["temp"]
    weather_description = weather_data["current"]["weather"][0]["description"]

    return {
        "weather": {
            "temperature": main_temp,
            "description": weather_description,
        }
    }
