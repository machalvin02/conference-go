import requests
from django.db import models
from django.urls import reverse
from events.keys import PEXELS_API_KEY

class State(models.Model):
    """
    The State model represents a US state with its name
    and abbreviation.

    State is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=20)
    abbreviation = models.CharField(max_length=2)

    def __str__(self):
        return f"{self.abbreviation}"

    class Meta:
        ordering = ("abbreviation",)


class Location(models.Model):
    """
    The Location model describes the place at which an
    Event takes place, like a hotel or conference center.
    """

    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    room_count = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    picture_url = models.URLField(null=True, blank=True)
    state = models.ForeignKey(
        State,
        related_name="+",
        on_delete=models.PROTECT,
    )

    def save_picture_url(self):
        if not self.picture_url:
            search_query = f"{self.city} {self.state.abbreviation}"
            headers = {
                "Authorization": PEXELS_API_KEY,
            }
            params = {
                "query": search_query,
                "per_page": 1,
            }
            response = requests.get("https://api.pexels.com/v1/search", headers=headers, params=params)
            data = response.json()

            if "photos" in data and len(data["photos"]) > 0:
                self.picture_url = data["photos"][0]["src"]["medium"]
                self.save()

    def get_api_url(self):
        return reverse("api_show_location", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)  # Default ordering for Location


class Conference(models.Model):
    """
    The Conference model describes a specific conference.
    """

    # Has a one-to-many relationship with presentations.Presentation
    # Has a one-to-many relationship with attendees.Attendee

    name = models.CharField(max_length=200)
    starts = models.DateTimeField()
    ends = models.DateTimeField()
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    max_presentations = models.PositiveSmallIntegerField()
    max_attendees = models.PositiveIntegerField()

    location = models.ForeignKey(
        Location,
        related_name="conferences",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_conference", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("starts", "name")  # Default ordering for Conference
